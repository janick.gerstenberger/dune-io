DUNE-IO
==============

DUNE-IO is a [Distributed and Unified Numerics Environment][1]
module which defines interfaces and provides implementations for advanced/flexible data I/O.

Dependencies
------------

DUNE-IO requires GCC (5+) or clang (3.8+) and depends on the following DUNE modules:

* [dune-common][10]

* [dune-grid][12]

The following DUNE modules are strongly suggested:

* [dune-fem][0]

Installation
------------

For a full explanation of the DUNE installation process please read
the [installation notes][2].

 [0]: https://www.dune-project.org/modules/dune-fem/
 [1]: https://www.dune-project.org
 [2]: https://www.dune-project.org/doc/installation/
 [10]: http://gitlab.dune-project.org/core/dune-common
 [12]: http://gitlab.dune-project.org/core/dune-grid
 [14]: http://gitlab.dune-project.org/core/dune-localfunctions
