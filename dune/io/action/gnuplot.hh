#ifndef DUNE_IO_ACTION_HH
#define DUNE_IO_ACTION_HH

// STL includes
#include <iostream>
#include <memory>
#include <type_traits>
#include <utility>

// dune-common inlcudes
#include <dune/common/hybridutilities.hh>
#include <dune/common/rangeutilities.hh>

// dune-io includes
#include <dune/io/format/gnuplot.hh>
#include <dune/io/dataset/dataset.hh>
#include <dune/io/common/utility.hh>
#include <dune/io/dataset/view.hh>


namespace Dune
{

  namespace IO
  {

    namespace Impl
    {

      template< class Writer, class DataSet,
                std::enable_if_t< not Policy::IsScalar< typename DataSet::Policy >::value, int > = 0 >
      void addGnuplotData ( std::shared_ptr< Writer >& writer, const DataSet& dataSet )
      {
        writer->addData( dataSet );
      }

      template< class Writer, class DataSet,
                std::enable_if_t< Policy::IsScalar< typename DataSet::Policy >::value, int > = 0 >
      void addGnuplotData ( std::shared_ptr< Writer >& writer, const DataSet& dataSet )
      {
        for( auto i : range( dataSet.fieldInfo().size() ) )
          addGnuplotData( writer, dataSet.component( i ) );
      }


      // Impl::generateAction
      // --------------------

      template< class GridPart >
      decltype(auto) generateAction ( std::shared_ptr< GnuplotWriter< GridPart > >& writer, const std::string& name )
      {
        return  [ gnu = std::move( writer ), name ] ( const std::string& fragment, const std::string& path ) {
            gnu->pwrite( name + fragment, path );
          };
      }

    } // namespace Impl


    // gnuplotWriter
    // -------------

    decltype(auto) gnuplotWriter( const std::string& name )
    {
      std::cerr << "Warning: Call to gnuplotWriter( \"" << name << "\", ... ) is generating a no-op call." << std::endl;
      return [] ( const std::string&, const std::string& ) {};
    }

    template< class ... DataSets >
    decltype(auto) gnuplotWriter ( const std::string& name, DataSets&& ... dataSets )
    {
      auto gridView = common( dataSets.gridView() ... );
      auto writer   = std::make_shared< GnuplotWriter< decltype( gridView ) > >( gridView );

      Hybrid::forEach( std::forward_as_tuple( dataSets ... ), [&] ( auto&& dataSet ) {
          Impl::addGnuplotData( writer, std::forward< decltype( dataSet ) >( dataSet ) );
        } );

      return Impl::generateAction( writer, name );
    }

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_ACTION_HH
