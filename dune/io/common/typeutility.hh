#ifndef DUNE_IO_COMMON_TYPEUTILITY_HH
#define DUNE_IO_COMMON_TYPEUTILITY_HH

// STL includes
#include <tuple>
#include <type_traits>

// dune-common includes
#include <dune/common/std/type_traits.hh>

// dune-io includes
#include <dune/io/common/typetraits.hh>


namespace Dune
{

  namespace IO
  {

  	// Decorate
    // --------

    template< template< class > class D, class T >
    struct Decorate : public TypeOf< D< T > > {};

    template< template< class > class D, template< class > class C, class T >
    struct Decorate< D, C< T > > : public TypeOf< C< D< T > > > {};

    template< template< class > class D, template< class ... > class C, class ... Ts >
    struct Decorate< D, C< Ts ... > > : public TypeOf< C< D< Ts > ... > > {};

    template< template< class > class D, class T >
    using Decorate_t = typename Decorate< D, T >::Type;


    // UniqueTuple
    // -----------

    namespace Impl
    {

      // AddIfNotContained
      // -----------------

      template< class, class > struct AddIfNotContained;

      template< class T, class ... Ts >
      struct AddIfNotContained< T, std::tuple< Ts ... > >
      {
        using Type = std::conditional_t< Std::disjunction< std::is_same< Ts, T > ... >::value,
                                         std::tuple< Ts ... >,
                                         std::tuple< Ts ..., T > >;
      };

      template< class T, class Tuple >
      using AddIfNotContained_t = typename AddIfNotContained< T, Tuple >::Type;


      // UniqueTupleImpl
      // ---------------

      template< class, class = std::tuple<> > struct UniqueTupleImpl;

      template< class T, class Tuple >
      struct UniqueTupleImpl : public TypeOf< Tuple > {};

      template< class T, class ... Ts, class Tuple >
      struct UniqueTupleImpl< std::tuple< T, Ts ... >, Tuple >
        : public UniqueTupleImpl< std::tuple< Ts ... >, AddIfNotContained_t< T, Tuple > >
      {};

    } // namespace Impl

    template< class ... Ts >
    using UniqueTuple = typename Impl::UniqueTupleImpl< std::tuple< Ts ... > >::Type;

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_COMMON_TYPEUTILITY_HH
