#ifndef DUNE_IO_SIMPLE_HH
#define DUNE_IO_SIMPLE_HH

#if HAVE_DUNE_FEM

// C++ includes
#include <string>
#include <utility>

// dune-common includes
#include <dune/common/exceptions.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/path.hh>

// dune-fem includes
#include <dune/fem/io/parameter.hh>
#include <dune/fem/misc/mpimanager.hh>
#include <dune/fem/solver/timeprovider.hh>

// dune-actipipe includes
#include <dune/io/action/gnuplot.hh>
#include <dune/io/action/vtk.hh>
#include <dune/io/action/vtx.hh>
#include <dune/io/dataset/dataset.hh>
#include <dune/io/manager.hh>
#include <dune/io/predicates/fem.hh>


namespace Dune
{

  namespace IO
  {

    template< class C = typename Fem::MPIManager::CollectiveCommunication >
    class SimpleOutput : protected Manager< C > {
      using ThisType = SimpleOutput< C >;
      using BaseType = Manager< C >;

    protected:
      using BaseType::count_;
      using BaseType::path_;

    public:
      using CollectiveCommunicationType = typename BaseType::Communication;

      template< class IOTuple >
      SimpleOutput ( Fem::TimeProviderBase& tp, IOTuple&& data,
                     const Fem::ParameterReader parameter = Fem::Parameter::container() )
        : ThisType( Fem::MPIManager::comm(), tp, std::forward< IOTuple >( data ), parameter )
      {}

      template< class IOTuple >
      SimpleOutput ( const CollectiveCommunicationType& comm, Fem::TimeProviderBase& tp, IOTuple&& data,
                     const Fem::ParameterReader parameter = Fem::Parameter::container() )
        : BaseType( comm,
                    concatPaths( parameter.getValue< std::string >( "fem.io.path", "./" ),
                                 parameter.getValue< std::string >( "fem.prefix", "." ) ),
                    FemDefault( tp, parameter ) )
      {
        using Length = std::tuple_size< std::decay_t< IOTuple > >;
        static_assert( Length::value > 0, "IOTuple is empty." );

        const auto name = parameter.getValue< std::string >( "fem.io.datafileprefix", "" );

        static const std::string types[] = { "ascii", "base64", "appended-raw", "appended-base64" };
        const auto type = static_cast< VTK::OutputType >( parameter.getEnum( "fem.io.vtk.type", types, 2 ) );

        auto indices = std::make_index_sequence< Length::value >{};

        static const std::string formats[] = { "vtk-cell", "vtk-vertex", "sub-vtk-cell", "binary" , "gnuplot" , "none" };
        switch ( parameter.getEnum( "fem.io.outputformat", formats, 1 ) )
        {
          case 0:
            {
              const auto mode = ( parameter.getValue< bool >( "fem.io.conforming", false ) ? VTK::conforming: VTK::nonconforming );
              BaseType::add( vtk( name, mode, type, std::forward< IOTuple >( data ), indices ) );
            }
            break;

          case 1:
            BaseType::add( vtx( name, type, std::forward< IOTuple >( data ), indices ) );
            break;

          case 2:
            {
              const auto level = parameter.getValue< int >( "fem.io.subsamplinglevel", 1 );
              BaseType::add( subvtk( name, level, type, std::forward< IOTuple >( data ), indices ) );
            }
            break;

          case 3:
            DUNE_THROW( NotImplemented, "Format 'binary' is not supported." );
            break;

          case 4:
            BaseType::add( gnuplot( name, std::forward< IOTuple >( data ), indices ) );
            break;

          case 5:
            if ( Fem::Parameter::verbose() )
              std::cout << "Format 'none': no output will be produced." << std::endl;
            break;

          default:
            DUNE_THROW( InvalidStateException, "Invalid output format chosen." );
        }
      }

      void write () { BaseType::call(); }
      void operator() () { write(); }

      int writeCount () const { return count_; }
      auto path () const -> const std::string& { return path_; }

    protected:
      template< class IOTuple, std::size_t ... Is >
      decltype(auto) gnuplot ( const std::string& name,
                               const IOTuple& data, std::index_sequence< Is ... > )
      {
        return gnuplotWriter( name, dataSet( *std::get< Is >( data ), "", scalar ) ... );
      }

      template< class IOTuple, std::size_t ... Is >
      decltype(auto) subvtk ( const std::string& name, int level, VTK::OutputType type,
                              const IOTuple& data, std::index_sequence< Is ... > )
      {
        return subsamplingVTKWriter( name, level, type, dataSet( *std::get< Is >( data ), "", scalar ) ... );
      }

      template< class IOTuple, std::size_t ... Is >
      decltype(auto) vtk ( const std::string& name, VTK::DataMode mode, VTK::OutputType type,
                           const IOTuple& data, std::index_sequence< Is ... > )
      {
        return vtkWriter( name, mode, type, dataSet( *std::get< Is >( data ), "", scalar ) ... );
      }

      template< class IOTuple, std::size_t ... Is >
      decltype(auto) vtx ( const std::string& name, VTK::OutputType type,
                           const IOTuple& data, std::index_sequence< Is ... > )
      {
        return vtxWriter( name, type, dataSet( *std::get< Is >( data ), "", scalar ) ... );
      }

    };

  } // namespace IO

} // namespace Dune

#endif // #if HAVE_DUNE_FEM

#endif // #ifndef DUNE_IO_SIMPLE_HH
