#ifndef DUNE_IO_WRITER_BINARY_HH
#define DUNE_IO_WRITER_BINARY_HH

// STL includes
#include <iomanip>
#include <sstream>
#include <string>

// dune-common includes
#include <dune/common/path.hh>
#include <dune/common/std/type_traits.hh>
#include <dune/common/typeutilities.hh>

#if HAVE_DUNE_FEM

// STL includes
#include <tuple>
#include <type_traits>

// dune-common includes
#include <dune/common/hybridutilities.hh>

// dune-fem includes
#include <dune/fem/function/common/discretefunction.hh>
#include <dune/fem/io/streams/binarystreams.hh>

// dune-io includes
#include <dune/io/common/utility.hh>

#endif // #if HAVE_DUNE_FEM


namespace Dune::IO
{

  namespace Impl
  {

    auto binaryFileName (const std::string& name, const std::string& path, int commRank, int commSize)
      -> std::string
    {
      std::ostringstream s;
      s << 's' << std::setw(4) << std::setfill('0') << commSize << '-';
      s << 'p' << std::setw(4) << std::setfill('0') << commRank << '-';
      s << name << ".bin";

      return concatPaths(path, s.str());
    }

#if HAVE_DUNE_FEM

    // Impl::HasStreamCapabilities
    // ---------------------------

    template<class T>
    struct HasStreamCapabilities : std::is_base_of<Fem::IsDiscreteFunction, std::decay_t<T>>
    {};


    // Impl::binaryWriter
    // ------------------

    template<class... DFs,
             std::enable_if_t<Std::conjunction<HasStreamCapabilities<std::decay_t<DFs>>...>::value, int> = 0>
    auto binaryWriter (const std::string& name, DFs&& ...dfs)
    {
      auto& comm = common(IO::comm(dfs)...);
      auto rank = comm.rank();
      auto size = comm.size();

      return [dfs = std::tie(dfs...), name, rank, size] (const std::string& fragment, const std::string& path) {
          Fem::BinaryFileOutStream file(binaryFileName(name + fragment, path, rank, size));
          Hybrid::forEach(dfs, [&file](auto&& df) { file << df; });
        };
    }

    template<class... DFs,
             std::enable_if_t<not Std::conjunction<HasStreamCapabilities<std::decay_t<DFs>> ... >::value, int> = 0>
    auto binaryWriter (const std::string& name, const DFs& ...dfs)
    {
      static_assert(Std::to_false_type<void_t<DFs...>>::value, "binaryWriter(...) is only supported for dune-fem discrete functions.");
    }


    // Impl::binaryReader
    // ------------------

    template<class... DFs,
             std::enable_if_t<Std::conjunction<HasStreamCapabilities<std::decay_t<DFs>>...>::value, int> = 0>
    auto binaryReader (const std::string& name, DFs&& ...dfs)
    {
      auto& comm = common(IO::comm(dfs)...);
      auto rank = comm.rank();
      auto size = comm.size();

      return [dfs = std::tie(dfs...), name, rank, size] (const std::string& fragment, const std::string& path) {
          Fem::BinaryFileInStream file(binaryFileName(name + fragment, path, rank, size));
          Hybrid::forEach(dfs, [&file](auto&& df) { file >> df; });
        };
    }

    template<class ...DFs,
             std::enable_if_t<not Std::conjunction<HasStreamCapabilities<std::decay_t<DFs>>...>::value, int> = 0>
    auto binaryReader (const std::string& name,  DFs&& ...dfs)
    {
      static_assert(Std::to_false_type<void_t<DFs...>>::value, "binaryReader(...) is only supported for dune-fem discrete functions.");
    }

#endif // #if HAVE_DUNE_FEM

  } // namespace Impl


  // binaryWriter & binaryReader
  // ---------------------------

#if HAVE_DUNE_FEM

  auto binaryWriter (const std::string& name)
  {
    std::cerr << "Warning: binaryWriter(\"" << name << "\", ...) is generating a no-op action." << std::endl;
    return [] (const std::string&, const std::string&) {};
  }

  template<class... Data>
  decltype(auto) binaryWriter (const std::string& name, Data&& ...data)
  {
    return Impl::binaryWriter(name, std::forward<Data>(data)...);
  }

  auto binaryReader (const std::string& name)
  {
    std::cerr << "Warning: binaryReader(\"" << name << "\", ...) is generating a no-op action." << std::endl;
    return [] (const std::string&, const std::string&) {};
  }

  template<class... Data>
  decltype(auto) binaryReader (const std::string& name, Data&& ...data)
  {
    return Impl::binaryReader(name, std::forward<Data>(data)...);
  }

#else // #if HAVE_DUNE_FEM

  template<class... Data>
  void binaryWriter (const std::string&, Data&& ...)
  {
    static_assert(Std::to_false_type<void_t<Data...>>::value, "binaryWriter(...) is only supported for dune-fem discrete functions.");
  }

  template<class... Data>
  void binaryReader (const std::string&, Data&& ...)
  {
    static_assert(Std::to_false_type<void_t<Data...>>::value, "binaryReader(...) is only supported for dune-fem discrete functions.");
  }

#endif // #else // #if HAVE_DUNE_FEM

} // namespace Dune::IO

#endif // #ifndef DUNE_IO_WRITER_BINARY_HH
