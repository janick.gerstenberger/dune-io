#ifndef DUNE_IO_FORMAT_GNUPLOT_HH
#define DUNE_IO_FORMAT_GNUPLOT_HH

// STL includes
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>

// dune-common includes
#include <dune/common/exceptions.hh>
#include <dune/common/path.hh>
#include <dune/common/rangeutilities.hh>

// dune-grid includes
#include <dune/grid/common/rangegenerators.hh>


namespace Dune
{

  namespace IO
  {

  	// GnuplotWriter
    // -------------

    template< class GV >
    class GnuplotWriter
    {
      using GridView = GV;

      using Entity 			= typename GridView::template Codim< 0 >::Entity;
      using Geometry 		= typename Entity::Geometry;
      using Coordinate 	= typename Geometry::LocalCoordinate;

      static constexpr int order = 1;

    protected:
      struct LocalFunction
      {
        struct WrapperBase
        {
          virtual ~WrapperBase () {}

          virtual void bind( const Entity& ) = 0;
          virtual void unbind () = 0;

          virtual void write ( const Coordinate&, std::ostream& ) const = 0;
        };

        template< class F >
        struct Wrapper : public WrapperBase
        {
          using Function = std::decay_t< F >;

          template< class _F >
          Wrapper ( _F&& f ) : f_( std::forward< _F >( f ) ) {}

          void bind ( const Entity& e ) override { f_.bind( e ); }
          void unbind () override { f_.unbind(); }

          void write ( const Coordinate& x, std::ostream& out ) const override { out << f_( x ); }

        private:
          Function f_;
        };

        template< class F >
        explicit LocalFunction ( F&& f )
          : f_{ std::make_unique< Wrapper< decltype( localFunction( std::forward< F >( f ) ) ) > >( localFunction( std::forward< F >( f ) ) ) }
        {}

        void bind ( const Entity& e ) { f_->bind( e ); }
        void unbind () { f_->unbind(); }

        void write ( const Coordinate& x, std::ostream& out ) const { f_->write( x, out ); }

        std::shared_ptr< WrapperBase > f_;
      };

    public:
      explicit GnuplotWriter ( const GridView& gridView ) : gridView_( gridView ) {}

      template< class F >
      void addData ( F&& f ) { data_.push_back( LocalFunction{ std::forward< F >( f ) } ); }
      void clear () { data_.clear(); }

      auto write ( const std::string& name ) { return write( name, comm().rank(), comm().size() ); }
      auto pwrite ( const std::string& name, const std::string& path ) { return pwrite( name, path, comm().rank(), comm().size() ); }

    protected:
      auto parallelName ( const std::string& name, const std::string& path, int commRank, int commSize ) const
        -> std::string
      {
        std::ostringstream s;
        s << 's' << std::setw(4) << std::setfill('0') << commSize << '-';
        s << 'p' << std::setw(4) << std::setfill('0') << commRank << '-';
        s << name << ".gnu";

        return concatPaths( path, s.str() );
      }

      auto serialName ( const std::string& name, const std::string& path ) -> std::string
      {
        return concatPaths( path, name + ".gnu" );
      }

      auto write ( const std::string& name, int commRank, int commSize ) -> std::string
      {
        if ( commSize > 1 )
          return pwrite( name, "", commRank, commSize );

        std::ofstream file;
        std::string fileName = serialName( name, "" );

        file.exceptions( std::ios_base::badbit | std::ios_base::failbit | std::ios_base::eofbit );

        try {
          file.open( fileName, std::ios::binary );
        }
        catch(...) {
          std::cerr << "Filename: " << fileName << " could not be opened." << std::endl;
          throw;
        }

        if (! file.is_open())
          DUNE_THROW(IOError, "Could not write to file " << fileName << ".");

        write( file );
        file.close();

        return fileName;
      }

      auto pwrite ( const std::string& name, const std::string& path, int commRank, int commSize ) -> std::string
      {
        std::ofstream file;
        std::string fileName = parallelName( name, path, commRank, commSize );

        file.exceptions( std::ios_base::badbit | std::ios_base::failbit | std::ios_base::eofbit );

        try {
          file.open( fileName, std::ios::binary );
        }
        catch(...) {
          std::cerr << "Filename: " << fileName << " could not be opened." << std::endl;
          throw;
        }

        if (! file.is_open())
          DUNE_THROW(IOError, "Could not write to file " << fileName << ".");

        write( file );
        file.close();
        comm().barrier();

        return fileName;
      }

    private:
      void write( std::ofstream& gnu )
      {
        gnu << std::scientific << std::setprecision( 16 );

        for ( const auto& entity : elements( gridView(), Partitions::interiorBorder ) )
        {
          auto geometry = entity.geometry();

          for ( auto i : range( geometry.corners() ) )
          {
          	auto x = geometry.corner( i );
            gnu << x;
            std::for_each( data_.begin(), data_.end(), [ & ] ( auto& f ) {
                f.bind( entity );
                gnu << " ";
                f.write( geometry.local( x ) , gnu );
                f.unbind();
              } );
            gnu << "\n";
          }
          gnu << "\n\n";
        }
      }

    protected:
      auto gridView () const -> const GridView& { return gridView_; }
      decltype(auto) comm() const { return gridView().comm(); }

      GridView gridView_;
      std::list< LocalFunction > data_;
    };

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_FORMAT_GNUPLOT_HH
