set(HEADERS
  default.hh
  fem.hh
)

install(FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/io/predicates)
