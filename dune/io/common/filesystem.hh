#ifndef DUNE_IO_COMMON_FILESYSTEM_HH
#define DUNE_IO_COMMON_FILESYSTEM_HH


#include <fstream>
#include <iostream>
#include <cstdio>

// sytem includes
#include <sys/stat.h>
#include <dirent.h>

// dune-common includes
#include <dune/common/exceptions.hh>


namespace Dune
{

  namespace IO
  {

    /*
     * Copied from dune/fem/io/io.cc from since we lack a fallback for C++17 Filesystem
     */

    // createDirectory
    // ---------------

    bool createDirectory ( const std::string &inName )
    {
      std::string name = inName;

      // strip of last character if it is a '/'
      if( name[ name.size() - 1 ] == '/' )
        name = name.substr( 0, name.size() -1 );

      // try to open directory (returns null pointer, if path does not exist)
      DIR *dir = opendir( name.c_str() );
      if( dir != 0 )
      {
        if( closedir( dir ) < 0 )
          std::cerr << "Error: Could not close directory." << std::endl;
        return true;
      }

      // try to create the father directory
      size_t pos = name.rfind( '/' );
      if( pos != std::string::npos )
      {
        const std::string father = name.substr( 0, pos );
        if( !createDirectory( father ) )
          return false;
      }

      // try to create the new child directory
      mode_t mode = S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH;
      return (mkdir( name.c_str(), mode ) >= 0);
    }


    // fileExists
    // ----------

    bool fileExists ( const std::string &name )
    {
      std::ifstream file( name );
      return file.is_open();
    }


    // directoryExists
    // ---------------

    bool directoryExists ( const std::string &name )
    {
      // if directory does not exist return false
      DIR* directory = opendir( name.c_str() );
      const bool exists = (directory != 0);
      // close directory again
      if ( exists )
      closedir( directory );
      return exists;
    }

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_COMMON_FILESYSTEM_HH
