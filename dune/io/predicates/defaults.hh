#ifndef DUNE_IO_PREDICATES_DEFAULT_HH
#define DUNE_IO_PREDICATES_DEFAULT_HH

// STL includes
#include <cstddef>


namespace Dune
{

  namespace IO
  {

    // Always
    // ------

    struct Always
    {
      bool operator() () const noexcept { return true; }
    };

    // Never
    // ------

    struct Never
    {
      bool operator() () const noexcept { return false; }
    };

    // EveryNth
    // --------

    struct EveryNth
    {
      explicit EveryNth ( std::size_t n ) : n_( n ) {}

      bool operator() () const noexcept { return (n_ > 0) && !(calls_ %= n_)++; }

    private:
      const std::size_t n_;
      mutable std::size_t calls_ = 0;
    };

    // Once
    // ------

    struct Once
    {
      bool operator() () const noexcept
      {
        if (!called)
        {
          called = true;
          return true;
        }

        return false;
      }

    private:
      mutable bool called = false;
    };

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_PREDICATES_DEFAULT_HH
