set(HEADERS
  manager.hh
  simple.hh
)

add_subdirectory(action)
add_subdirectory(common)
add_subdirectory(dataset)
add_subdirectory(format)
add_subdirectory(predicates)
add_subdirectory(test EXCLUDE_FROM_ALL)

install(FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/io)
