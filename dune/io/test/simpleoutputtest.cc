# include <config.h>

// C++ includes
#include <iostream>
#include <string>
#include <sstream>

// dune-common includes
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

// dune-geometry includes
#include <dune/geometry/dimension.hh>

// dune-grid includes
#include <dune/grid/yaspgrid.hh>

// dune-fem includes
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/gridpart/leafgridpart.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/common/interpolate.hh>
#include <dune/fem/space/discontinuousgalerkin/space.hh>

// dune-io includes
#include <dune/io/simple.hh>


using Grid      = Dune::YaspGrid< 2 >;
using GridPart  = Dune::Fem::LeafGridPart< Grid >;

using FunctionSpace         = Dune::Fem::GridFunctionSpace< GridPart, Dune::Dim< 3 > >;
using DiscreteFunctionSpace = Dune::Fem::DiscontinuousGalerkinSpace< FunctionSpace, GridPart, 2 >;
using DiscreteFunction      = Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpace >;


// ExampleFunction
// ---------------

struct ExampleFunction
{
  using FunctionSpaceType = FunctionSpace;
  using DomainType        = typename FunctionSpaceType::DomainType;
  using RangeType         = typename FunctionSpaceType::RangeType;
  using JacobianRangeType = typename FunctionSpaceType::JacobianRangeType;

  void evaluate ( const DomainType &x, RangeType &value ) const
  {
    value = 1.0;
    for( int k = 0; k < FunctionSpace::dimDomain; ++k )
      value *= sin( 2.0 * M_PI * x[ k ] );
  }
};


// main
// ----

int main(int argc, char** argv)
try
{
  Dune::Fem::MPIManager::initialize( argc, argv );

  auto parameter = Dune::Fem::Parameter::container();
  parameter.append( argc, argv );

  Grid grid( { 1.0, 1.0 }, { 4, 4 } );
  GridPart gridPart( grid );

  DiscreteFunctionSpace space( gridPart );
  DiscreteFunction df( "name", space );

  ExampleFunction example;
  auto adapter = gridFunctionAdapter( "analytical", example, gridPart, 3 );
  Dune::Fem::interpolate( adapter, df );

  Dune::Fem::FixedStepTimeProvider<> tp( 0.0, 0.01 );
  Dune::IO::SimpleOutput<> output( tp, std::make_tuple( &df, &adapter ) );

  for( ; tp.time() < 1.0; tp.next() )
  {
    /* do something with df */
    output();
  }
  output();

  return 0;
}
catch( const Dune::Exception &e )
{
  std::cerr << "Dune Exception: " << e << std::endl;
  return 1;
}
catch( const std::exception &e )
{
  std::cerr << "STL Exception: " << e.what() << std::endl;
  return 1;
}
catch( ... )
{
  std::cerr << "Unkown Exception!" << std::endl;
}
