#include <config.h>

// STL includes
#include <complex>
#include <iostream>

// dune-common includes
#include <dune/common/fvector.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/optional.hh>

// dune-grid includes
#include <dune/grid/common/rangegenerators.hh>
#include <dune/grid/yaspgrid.hh>

#if HAVE_DUNE_FEM
// dune-fem includes
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/gridpart/leafgridpart.hh>
#include <dune/fem/misc/mpimanager.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/common/interpolate.hh>
#include <dune/fem/space/finitevolume/space.hh>
#endif // #if HAVE_DUNE_FEM

// dune-io includes
#include <dune/io/dataset/dataset.hh>
#include <dune/io/dataset/view.hh>


using Grid = Dune::YaspGrid<2>;
using Range = Dune::FieldVector< typename Grid::ctype, Grid::dimensionworld >;

template< class ... Args >
decltype(auto) generateDataSets ( Args&& ... args )
{
	using namespace Dune::IO;

	return std::make_tuple(
			dataSet( args ..., "vectorial" /*, vectorial */ ),
			dataSet( args ..., "scalar", scalar ).component( 0 ),
			dataSet( args ..., "scalar", scalar ).component( 1 ),
			dataSet( args ..., "swapped", [] ( const Range& v ) { return Range{ v[ 1 ], v[ 0 ] }; } ),
			dataSet( args ..., "complex", [] ( const Range& v ) { return std::complex< double >{ v[ 0 ], v[ 1 ] }; } )
		);
}

template< class DataSet, class Result >
auto checkResult ( const DataSet&, const Range&, const Result&, std::integral_constant< bool, false > )
{
	return true;
}

template< class DataSet, class Result >
auto checkResult ( const DataSet&, const Range& range, const Result& result, std::integral_constant< bool, true > )
	-> std::enable_if_t< not Dune::IO::Policy::IsTransformation< typename DataSet::Policy >::value, bool >
{
	return range == result;
}

template< class DataSet, class Result >
auto checkResult ( const DataSet& dataSet, const Range& range, const Result& result, std::integral_constant< bool, true > )
	-> std::enable_if_t< Dune::IO::Policy::IsTransformation< typename DataSet::Policy >::value, bool >
{
	return dataSet.transformation()( range ) == result;
}


template< class DataSet, bool B = true >
bool testLocalFunction ( const DataSet& dataSet, std::integral_constant< bool, B > checked = {} )
{
	auto localView = localFunction( dataSet );

	bool success = true;
	for( auto&& entity : Dune::elements( dataSet.gridView() ) )
	{
		localView.bind( entity );
		auto geometry = entity.geometry();
		auto x = geometry.center();
		success &= checkResult( dataSet, x, localView( geometry.local( x ) ), checked );
		localView.unbind();
	}

	std::cout << " " << dataSet.name() << ": ";
	if ( checked )
		std::cout << success ;
	else
		std::cout << "values not checked.";
	std::cout << std::endl;
	return success;
}

/*
 *	test DataSet for a vector-like data payloads
 */

template< class Grid >
bool testVectorDataSet ( Grid& grid )
{
	auto gridView = grid.leafGridView();
	std::vector< Range > data( gridView.size( 0 ) );

	for ( auto&& entity : elements( gridView ) )
		data[ gridView.indexSet().index( entity ) ] = entity.geometry().center();

	std::cout << " === Testing DataSets for vector-like containers.\n" << std::boolalpha << std::endl;

	bool success = true;
	auto dataSets = generateDataSets( gridView, data );
	Dune::Hybrid::forEach( dataSets, [ &success ] ( auto&& dataSet ) {
			success &= testLocalFunction( std::forward< decltype( dataSet ) >( dataSet ) );
		} );

	std::cout << std::noboolalpha << std::endl;
	return success;
}

/*
 *	test DataSet for localFunction-enabled payloads
 */

template< class GV, class R >
struct FunctionDummy
{
	using GridView = GV;
	using Range = R;

	using Entity = typename GridView::template Codim< 0 >::Entity;
	using Geometry = typename Entity::Geometry;
	using LocalCoordinate = typename Geometry::LocalCoordinate;

	explicit FunctionDummy ( const GridView& gridView ) : gridView_( gridView ) {}

	auto gridView () const -> const GridView& { return gridView_; }
	void bind ( const Entity& entity ) { geometry_.emplace( entity.geometry() ); }
	void unbind () { geometry_.reset(); }

	auto operator() ( const LocalCoordinate& x ) const -> Range { assert( geometry_ ); return geometry_->global( x ); }

private:
	GridView gridView_;
	Dune::Std::optional< Geometry > geometry_;
};

template< class GV, class R >
auto localFunction ( const FunctionDummy< GV, R >& function ) -> FunctionDummy< GV, R > { return function; }

template< class Grid >
bool testFunctionDataSet ( Grid& grid )
{
	auto gridView = grid.leafGridView();
	FunctionDummy< decltype( gridView ), Range > function( gridView );

	std::cout << " === Testing DataSets for localFunction-enabled functions.\n" << std::boolalpha << std::endl;

	bool success = true;
	auto dataSets = generateDataSets( function );
	Dune::Hybrid::forEach( dataSets, [ &success ] ( auto&& dataSet ) {
			success &= testLocalFunction( std::forward< decltype( dataSet ) >( dataSet ) );
		} );

	std::cout << std::noboolalpha << std::endl;
	return success;
}

/*
 *	test DataSet for dune-fem grid function payloads
 */

template< class FunctionSpace >
struct IdentityFunction
{
  using FunctionSpaceType = FunctionSpace;

  using DomainType 				= typename FunctionSpaceType::DomainType;
  using RangeType 				= typename FunctionSpaceType::RangeType;
  using JacobianRangeType = typename FunctionSpaceType::JacobianRangeType;

  void evaluate ( const DomainType &x, RangeType &value ) const { value = x; }
};

template< class Grid >
bool testFemFunctionDataSet ( Grid& grid )
{
#if HAVE_DUNE_FEM
	using GridPart 							= Dune::Fem::LeafGridPart< Grid >;
	using FunctionSpace 				= Dune::Fem::GridFunctionSpace< GridPart, Range >;
	using DiscreteFunctionSpace = Dune::Fem::FiniteVolumeSpace< FunctionSpace, GridPart >;
	using DiscreteFunction 			= Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpace >;

	GridPart gridPart( grid );
	DiscreteFunctionSpace space( gridPart );
	DiscreteFunction function( "function", space );
	IdentityFunction< FunctionSpace > identity;

	auto adapter = gridFunctionAdapter( identity, gridPart, 2 );
	interpolate( adapter, function );

	bool success1 = true, success2 = true;
	std::cout << " === Testing DataSets for dune-fem discrete functions.\n" << std::boolalpha << std::endl;
	{
		auto dataSets = generateDataSets( function );
		Dune::Hybrid::forEach( dataSets, [ &success1 ] ( auto&& dataSet ) {
				success1 &= testLocalFunction( std::forward< decltype( dataSet ) >( dataSet ) );
			} );

		auto divergence = Dune::IO::dataSet(
		  function, "divergence",
		  [] ( const typename FunctionSpace::JacobianRangeType& jac ) {
		    return jac[ 0 ][ 0 ] + jac[ 1 ][ 1 ];
		  } );
		testLocalFunction( divergence, std::integral_constant< bool, false >{} );

		std::cout << std::noboolalpha << std::endl;
	}

	std::cout << " === Testing DataSets for dune-fem grid functions.\n" << std::boolalpha << std::endl;
	{
		auto dataSets = generateDataSets( adapter );
		Dune::Hybrid::forEach( dataSets, [ &success2 ] ( auto&& dataSet ) {
				success2 &= testLocalFunction( std::forward< decltype( dataSet ) >( dataSet ) );
			} );

		std::cout << std::noboolalpha << std::endl;
	}

	return success1 && success2;
#else // #if HAVE_DUNE_FEM
	std::cout << "Module dune-fem not found. Skipping dune-fem specific tests.\n" << std::endl;
	return true;
#endif // #else // #if HAVE_DUNE_FEM
}


// main
// ----

int main(int argc, char** argv)
{
#if HAVE_DUNE_FEM
	Dune::Fem::MPIManager::initialize( argc, argv );
#else // #if HAVE_DUNE_FEM
  Dune::MPIHelper::instance( argc, argv );
#endif // #else // #if HAVE_DUNE_FEM

  Grid grid({1., 1.}, {2, 2});

  bool b = true;
  b &= testVectorDataSet( grid );
  b &= testFunctionDataSet( grid );
  b &= testFemFunctionDataSet( grid );

  return !b;
}
