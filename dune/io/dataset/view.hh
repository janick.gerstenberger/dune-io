#ifndef DUNE_IO_DATASET_VIEW_HH
#define DUNE_IO_DATASET_VIEW_HH

// STL includes
#include <type_traits>
#include <utility>

// dune-common includes
#include <dune/common/std/type_traits.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/typeutilities.hh>

// dune-fem includes
#if HAVE_DUNE_FEM
#include <dune/fem/function/localfunction/const.hh>
#endif //HAVE_DUNE_FEM

// dune-io includes
#include <dune/io/common/typetraits.hh>
#include <dune/io/dataset/policy.hh>


namespace Dune
{

  namespace IO
  {

    // forward declarations
    // --------------------

    template<class, class, class> class DataSet;


    // LocalVectorView
    // ---------------

    template<class GV, class D, template<class> class Deco = AddFieldVector_t>
    struct LocalIndexVectorView
    {
      using GridView = GV;
      using Data = D;

      using Entity  = typename GridView::template Codim<0>::Entity;
      using Index   = typename GridView::IndexSet::IndexType;
      using Domain  = typename Entity::Geometry::LocalCoordinate;
      using Range   = Deco<Range_t<GridView, Data>>;

      LocalIndexVectorView (const GridView& gridView, const Data& data) : gridView_(gridView), data_(data) {}

      void bind (const Entity& entity) { index_ = gridView_.indexSet().index(entity); }
      void unbind () { index_ = -1; }
      auto operator() (const Domain& x) const -> Range { assert(index_ < data_.size()); return data_[index_]; }

    private:
      GridView gridView_;
      const Data& data_;
      Index index_ = -1;
    };


    template<class GV, class D, template<class> class Deco = AddFieldVector_t>
    struct LocalEntityVectorView
    {
      using GridView = GV;
      using Data = D;

      using Entity  = typename GridView::template Codim<0>::Entity;
      using Domain  = typename Entity::Geometry::LocalCoordinate;
      using Range   = Deco<Range_t<GridView, Data>>;

      LocalEntityVectorView (const GridView& gridView, const Data& data) : gridView_(gridView), data_(data) {}

      void bind (const Entity& entity) { entity_ = &entity; }
      void unbind () { entity_ = nullptr; }
      auto operator() (const Domain& x) const -> Range { assert(entity_); return data_[*entity_]; }

    private:
      GridView gridView_;
      const Data& data_;
      const Entity* entity_ = nullptr;
    };


#if HAVE_DUNE_FEM

    // FemLocalFunctionView
    // --------------------

    template <class D>
    struct FemLocalFunctionView
    {
      using Data = Fem::ConstLocalFunction<D>;

      using Entity        = typename Data::EntityType;
      using Domain        = typename Data::DomainType;
      using Range         = typename Data::RangeType;
      using JacobianRange = typename Data::JacobianRangeType;

      explicit FemLocalFunctionView ( const D& data ) : data_{data} {};

      void bind (const Entity& entity) { data_.bind(entity); }
      void unbind () { data_.unbind(); }

      auto operator() (const Domain& x) const -> Range { return data_.evaluate(x); }
      auto jacobian (const Domain& x) const -> JacobianRange { return data_.jacobian(x); }

    private:
      Data data_;
    };

#endif // #if HAVE_DUNE_FEM


    // TranformationView
    // -----------------

    template<class LV, class F>
    struct TransformationView
    {
      using LocalView = LV;
      using Transformation = F;

      using Range = AddFieldVector_t<ReturnType_t<F>>;

      TransformationView (LocalView view, Transformation f) : view_(std::move(view)), f_(std::move(f)) {}

      template<class Entity>
      auto bind (const Entity& entity)
        -> void_t<decltype(std::declval<LocalView>().bind(entity))>
      {
        view_.bind(entity);
      }

      auto unbind () { view_.unbind(); }

      template<class Domain>
      auto operator() (const Domain& x) const
        -> std::enable_if_t<Std::is_callable<LV(const Domain&)>::value, Range>
      {
        return evaluate(view_, f_, x, PriorityTag< 42 >{});
      }

    private:
      template<class _F, class Domain>
      using Range_t = decltype(std::declval<const _F>()(std::declval<const Domain>()));

      template<class _F, class Domain>
      using Jacobian_t = decltype(std::declval<const _F>().jacobian(std::declval<const Domain>()));

      template<class View, class _F, class Domain>
      static auto evaluate (const View& view, _F&& f, const Domain& x, PriorityTag<2>)
       -> std::enable_if_t<Std::is_callable<_F(Range_t<View, Domain>), Range>::value, Range>
      {
        return f(view(x));
      }

      template< class View, class _F, class Domain >
      static auto evaluate (const View& view, _F&& f, const Domain& x, PriorityTag<1>)
       -> std::enable_if_t<Std::is_detected<Jacobian_t, View, Domain >::value &&
                           Std::is_callable<_F(Jacobian_t<View, Domain>), Range>::value,
                           Range>
      {
        return f(view.jacobian(x));
      }

      template<class View, class _F, class Domain>
      static auto evaluate (const View& view, _F&& f, const Domain& x, PriorityTag<0>)
       -> std::enable_if_t<Std::is_detected<Jacobian_t, View, Domain>::value &&
                           Std::is_callable<_F(Range_t<View, Domain>, Jacobian_t<View, Domain>), Range>::value,
                           Range>
      {
        return f(view(x), view.jacobian(x));
      }

      LocalView view_;
      Transformation f_;
    };


    // localFunction
    // -------------

    namespace Impl
    {

      // localFunction_helper & localFunction_t
      // --------------------------------------

      template<class T>
      using LocalContext = decltype(std::declval<const T>().localContext());

      template< class T >
      using LocalFunction = decltype(localFunction(std::declval<const T>()));

      struct Pass {};
      struct Wrap {};

      template<class DataSet, class Tag = Wrap,
               std::enable_if_t<Std::is_detected<LocalContext, typename DataSet::Data>::value, int> = 0>
      decltype(auto) localFunction_helper (PriorityTag<6>, const DataSet& dataSet, Tag = {})
      {
        return dataSet.data();
      }


      template<class DataSet, class Tag = Wrap,
               std::enable_if_t<Std::is_detected<LocalFunction, typename DataSet::Data>::value, int> = 0>
      decltype(auto) localFunction_helper (PriorityTag<5>, const DataSet& dataSet, Tag = {})
      {
        return localFunction(dataSet.data());
      }

#if HAVE_DUNE_FEM
      template<class DataSet, class Tag = Wrap,
               std::enable_if_t<Std::is_detected<Fem::ConstLocalFunction, typename DataSet::Data>::value , int> = 0>
      auto localFunction_helper (PriorityTag<4>, const DataSet& dataSet, Tag = {})
      {
        return FemLocalFunctionView<typename DataSet::Data>{ dataSet.data() };
      }
#endif // #if HAVE_DUNE_FEM


      template<class DataSet,
               std::enable_if_t<IsIndexable<typename DataSet::Data, typename DataSet::GridView::template Codim<0>::Entity>::value, int> = 0>
      auto localFunction_helper (PriorityTag<3>, const DataSet& dataSet, Wrap = {})
        -> LocalEntityVectorView<typename DataSet::GridView, typename DataSet::Data>
      {
        return {dataSet.gridView(), dataSet.data()};
      }


      template<class DataSet,
               std::enable_if_t<IsIndexable<typename DataSet::Data, typename DataSet::GridView::IndexSet::IndexType>::value, int> = 0>
      auto localFunction_helper (PriorityTag<2>, const DataSet& dataSet, Wrap = {})
        -> LocalIndexVectorView<typename DataSet::GridView, typename DataSet::Data>
      {
        return {dataSet.gridView(), dataSet.data()};
      }

      template<class DataSet,
               std::enable_if_t<IsIndexable<typename DataSet::Data, typename DataSet::GridView::template Codim<0>::Entity>::value, int> = 0>
      auto localFunction_helper (PriorityTag<1>, const DataSet& dataSet, Pass)
        -> LocalEntityVectorView<typename DataSet::GridView, typename DataSet::Data, TypeOf_t>
      {
        return {dataSet.gridView(), dataSet.data()};
      }

      template<class DataSet,
               std::enable_if_t<IsIndexable<typename DataSet::Data, typename DataSet::GridView::IndexSet::IndexType>::value, int> = 0>
      auto localFunction_helper (PriorityTag<0>, const DataSet& dataSet, Pass)
        -> LocalIndexVectorView<typename DataSet::GridView, typename DataSet::Data, TypeOf_t>
      {
        return {dataSet.gridView(), dataSet.data()};
      }

      template<class T, class S = Wrap>
      using localFunction_t = std::decay_t<decltype(localFunction_helper(PriorityTag<42>{}, std::declval<const T>(), S{}))>;


      // Impl::localFunction
      // -------------------

      template<class DataSet>
      decltype(auto) localFunction (const DataSet& data, Policy::Scalar)
      {
        static_assert(Std::to_false_type<DataSet>::value,
                      "Calling localFunction(...) on a DataSet with policy Scalar is invalid.");
      }

      template<class DataSet>
      auto localFunction (const DataSet& data, Policy::Transformation)
        -> TransformationView<localFunction_t<DataSet, Pass>, decltype(data.transformation())>
      {
        return {localFunction_helper(PriorityTag<42>{}, data, Pass{}), data.transformation()};
      }

      template<class DataSet>
      auto localFunction (const DataSet& data, Policy::Vectorial)
        -> localFunction_t<DataSet>
      {
        return localFunction_helper(PriorityTag<42>{}, data);
      }

    } // namespace Impl


    template<class GV, class D, class P>
    decltype(auto) localFunction (const DataSet<GV, D, P>& dataSet)
    {
      return Impl::localFunction(dataSet, dataSet.policy());
    }


  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_DATASET_VIEW_HH
