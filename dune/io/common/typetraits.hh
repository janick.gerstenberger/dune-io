#ifndef DUNE_IO_COMMON_TYPETRAITS_HH
#define DUNE_IO_COMMON_TYPETRAITS_HH

// C++ includes
#include <tuple>
#include <type_traits>
#include <utility>

// dune-common includes
#include <dune/common/fvector.hh>
#include <dune/common/std/type_traits.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/typeutilities.hh>


namespace Dune
{

  // forward declarations
  // --------------------

  template<class> class GridView;


  namespace IO
  {

    // TypeOf
    // ------

    template<class T>
    struct TypeOf
    {
      using Type = T;
    };

    template<class T>
    using TypeOf_t = typename TypeOf<T>::Type;


    // IsSimpleFunctor
    // ---------------

    namespace Impl
    {

      template<class  T>
      using SimpleFunctor_t = decltype(&std::decay_t<T>::operator());

    } // namespace Impl

    template<class T>
    using IsSimpleFunctor = Std::is_detected<Impl::SimpleFunctor_t, T>;


    // IsVector
    // --------

    namespace Impl
    {

      template<class T, class U>
      using Indexable_t = decltype(std::declval<T>()[std::declval<U>()]);

    } // namespace Impl

    template<class T, class U = std::size_t>
    using IsIndexable = Std::is_detected<Impl::Indexable_t, T, U>;





    // ReturnType & ReturnType_t
    // -------------------------

    namespace Impl
    {

      template<class T>
      struct ReturnTypeImpl
      {
        static_assert(Std::to_false_type<T>::value, "T is not a Callable.");
      };

      template<class R, class... Args>
      struct ReturnTypeImpl<R (*)(Args...)> : public TypeOf<R> {};

      template<class T, class R, class... Args>
      struct ReturnTypeImpl<R (T::*) (Args...)> : public TypeOf<R> {};

      template<class T, class R, class... Args>
      struct ReturnTypeImpl<R (T::*)(Args...) const> : public TypeOf<R> {};

    } // namespace Impl

    template<class T>
    struct ReturnType : public Impl::ReturnTypeImpl<Std::detected_t<Impl::SimpleFunctor_t, T>> {};

    template<class T>
    using ReturnType_t = typename ReturnType<T>::Type;


    // AddFieldVector
    // --------------

    namespace Impl
    {

      template<class T, class = void>
      struct AddFieldVectorImpl
      {
        static_assert(Std::to_false_type<T>::value, "Invalid type `FieldVector<T, 1>`.");
      };

      template<class K, int N>
      struct AddFieldVectorImpl<FieldVector<K, N>> : public TypeOf<FieldVector<K, N>> {};

      template<class T>
      struct AddFieldVectorImpl<T, std::enable_if_t<IsNumber<T>::value>> : public TypeOf<FieldVector<T, 1>> {};

    } // namespace Impl

    template<class T>
    struct AddFieldVector : public Impl::AddFieldVectorImpl<T> {};

    template<class T>
    using AddFieldVector_t = typename AddFieldVector<T>::Type;


    // Range_t
    // -------

    namespace Impl
    {
      template<class T, class U>
      auto range_helper (PriorityTag<3>, const T&, const U&) -> typename U::RangeType;

      template<class T, class U>
      auto range_helper (PriorityTag<2>, const T&, const U&) -> typename U::Range;

      template<class T, class U>
      auto range_helper (PriorityTag<1>, const T&, const U&)
        -> std::decay_t<Indexable_t<U, typename T::template Codim<0>::Entity>>;

      template<class T, class U>
      auto range_helper (PriorityTag<0>, const T&, const U&)
        -> std::decay_t<Indexable_t<U, typename T::IndexSet::IndexType>>;

    } // namespace Impl

    template<class T, class U>
    using Range_t = decltype(Impl::range_helper(PriorityTag< 42 >{}, std::declval<const T&>(), std::declval<const U&>()));


    // IsGridView
    // ----------

    namespace Impl
    {

      template<class T>
      using Traits_t = typename T::Traits;

    } // namespace Impl

    template<class T>
    using IsGridView = Std::conjunction<Std::is_detected<Impl::Traits_t, T>,
                                        std::is_base_of<GridView<Impl::Traits_t<T>>, T>>;

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_COMMON_TYPETRAITS_HH
