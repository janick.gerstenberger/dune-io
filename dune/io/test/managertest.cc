# include <config.h>

// C++ includes
#include <iostream>
#include <string>
#include <sstream>

// dune-common includes
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>

// dune-geometry includes
#include <dune/geometry/dimension.hh>

// dune-grid includes
#include <dune/grid/yaspgrid.hh>

// dune-fem includes
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/gridpart/leafgridpart.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/space/common/functionspace.hh>
#include <dune/fem/space/common/interpolate.hh>
#include <dune/fem/space/discontinuousgalerkin/space.hh>

// dune-io includes
#include <dune/io/action/binary.hh>
#include <dune/io/action/gnuplot.hh>
#include <dune/io/action/vtk.hh>
#include <dune/io/action/vtx.hh>
#include <dune/io/dataset/dataset.hh>
#include <dune/io/manager.hh>
#include <dune/io/predicates/fem.hh>


using Grid      = Dune::YaspGrid< 2 >;
using GridPart  = Dune::Fem::LeafGridPart< Grid >;
using GridView  = typename GridPart::GridViewType;

using FunctionSpace         = Dune::Fem::GridFunctionSpace< GridPart, Dune::Dim< 3 > >;
using DiscreteFunctionSpace = Dune::Fem::DiscontinuousGalerkinSpace< FunctionSpace, GridPart, 2 >;
using DiscreteFunction      = Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpace >;


// ExampleFunction
// ---------------

struct ExampleFunction
{
  using FunctionSpaceType = FunctionSpace;
  using DomainType        = typename FunctionSpaceType::DomainType;
  using RangeType         = typename FunctionSpaceType::RangeType;
  using JacobianRangeType = typename FunctionSpaceType::JacobianRangeType;

  void evaluate ( const DomainType &x, RangeType &value ) const
  {
    value = 1.0;
    for( int k = 0; k < FunctionSpace::dimDomain; ++k )
      value *= sin( 2.0 * M_PI * x[ k ] );
  }
};


// main
// ----

int main(int argc, char** argv)
try
{
  Dune::Fem::MPIManager::initialize( argc, argv );

  auto parameter = Dune::Fem::Parameter::container();
  parameter.append( argc, argv );

  Grid grid( { 1.0, 1.0 }, { 4, 4 } );
  GridPart gridPart( grid );
  GridView gridView = static_cast< GridView >( gridPart );

  DiscreteFunctionSpace space( gridPart );
  DiscreteFunction df( "name", space );

  ExampleFunction example;
  auto adapter = gridFunctionAdapter( "analytical", example, gridPart, 3 );
  Dune::Fem::interpolate( adapter, df );

  std::vector< Dune::FieldVector< double, 3 > > vec( gridPart.indexSet().size( 0 ), Dune::FieldVector< double, 3 >{ 0.0, 0.0, 1.0 } );

  using Range     = typename FunctionSpace::RangeType;
  using Jacobian  = typename FunctionSpace::JacobianRangeType;

   // create DataSets
  using Dune::IO::dataSet;
  auto&& solution   = dataSet( df, "solution" );
  auto&& scalar     = dataSet( df, "scalar", Dune::IO::scalar );
  auto&& analytical = dataSet( adapter, "analytical" );
  auto&& rotated    = dataSet( df, "rotated", [] ( const Range& v ) { return Range{ v[ 1 ], -v[ 0 ], v[ 2 ] }; } );
  auto&& divergence = dataSet( df, "divergence", [] ( const Jacobian& jac ) { return jac[ 0 ][ 0 ] + jac[ 1 ][ 1 ]; } );
  auto&& normals    = dataSet( gridView, vec, "normals" );

  {
    using namespace Dune::VTK;
    using namespace Dune::IO;
    Dune::Fem::FixedStepTimeProvider<> tp( 0.0, 0.01 );
    std::cout << "\n-- constructing \'output\':\n";

     // create OutputManager with the default dune-fem behaviour
    Dune::IO::Manager<> output( parameter.getValue< std::string >( "fem.prefix" ), true, Dune::IO::FemDefault( tp, parameter ) );

     // insert 'writer' callables into output
    output.add(
       vtkWriter( "base", nonconforming, ascii, solution, scalar, analytical, rotated, divergence, normals )
      ,subsamplingVTKWriter( "subsampled", 2, appendedraw, solution, scalar, analytical, rotated, divergence )
      ,vtxWriter( "vtx-projection", base64, solution, scalar, analytical, rotated, divergence )
      ,gnuplotWriter( "base", solution, scalar, analytical, rotated, divergence, normals )
      ,binaryWriter( "binary", df )
    );

    output.add( vtkWriter( "empty" ) );

    std::cout << "\n-- calling \'output\':\n";

    for( ; tp.time() <= 1.0; tp.next() )
    {
      /* do something with df */
      output();
    }
    output();

    output.clear();
  }

  return 0;
}
catch( const Dune::Exception &e )
{
  std::cerr << "Dune Exception: " << e << std::endl;
  return 1;
}
catch( const std::exception &e )
{
  std::cerr << "STL Exception: " << e.what() << std::endl;
  return 1;
}
catch( ... )
{
  std::cerr << "Unkown Exception!" << std::endl;
}
