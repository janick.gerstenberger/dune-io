#ifndef DUNE_IO_COMMON_UTILITY_HH
#define DUNE_IO_COMMON_UTILITY_HH

// C++ includes
#include <tuple>
#include <type_traits>
#include <utility>

// dune-common includes
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/type_traits.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/typeutilities.hh>

// dune-io includes
#include <dune/io/common/typetraits.hh>


namespace Dune
{

  namespace IO
  {

    // gridView
    // --------

    namespace Impl
    {

      template<class T>
      auto gridView (const T& t, PriorityTag<1>) -> decltype(t.gridView()) { return t.gridView(); }

      template<class T>
      using GridPart_t = decltype(std::declval< const T >().gridPart());

      template<class T, std::enable_if_t<Std::is_detected<GridPart_t, T>::value, int> = 0>
      auto gridView (const T& t, PriorityTag<0>)
      {
        return static_cast<typename std::decay_t<GridPart_t<T>>::GridViewType>(t.gridPart());
      }

    } // namespace Impl

    template<class T>
    decltype(auto) gridView(const T& t) { return Impl::gridView(t, PriorityTag<42>{}); }


    namespace Impl
    {

      template<class T>
      auto name (const T& t, PriorityTag<1>) -> decltype(t.name()) { return t.name(); }

      template<class T>
      auto name (const T&, PriorityTag<0>) { return std::string{}; }

    } // namespace Impl

    template<class T>
    auto name (const T& t) -> std::string { return Impl::name(t, PriorityTag<42>{}); }


    // order
    // -----

    namespace Impl
    {

      template<class T>
      using Order_t = decltype(std::declval<T>().order());

      template< class T, std::enable_if_t<Std::is_detected<Order_t, T>::value, int> = 0>
      int order_impl (T&& t, PriorityTag<3>) { return t.order(); }

      template<class T>
      using SpaceOrder_t = decltype(std::declval<T>().space().order());

      template<class T, std::enable_if_t< Std::is_detected<SpaceOrder_t, T>::value, int> = 0>
      int order_impl (T&& t, PriorityTag<2>) { return t.space().order(); }

      template<class T, std::enable_if_t<IsVector< T >::value>, int = 0>
      int order_impl (T&&, PriorityTag<1>) { return 0; }

      template<class T>
      int order_impl (T&&, PriorityTag<0>) { return -1; }

    } // namespace Impl

    template<class T>
    int order (T&& t) { return Impl::order_impl(std::forward<T>(t), PriorityTag<42>{}); }


    namespace Impl
    {

      template<class T>
      auto comm(const T& t, PriorityTag<3>) -> decltype(t.comm())
      {
        return t.comm();
      }

      template<class T>
      auto comm(const T& t, PriorityTag<2>) -> decltype(t.gridView().comm())
      {
        return t.gridView().comm();
      }

      template<class T>
      auto comm(const T& t, PriorityTag<1>) -> decltype(t.gridPart().comm())
      {
        return t.gridPart().comm();
      }

      template<class T>
      auto comm(const T&, PriorityTag<0>) -> std::add_lvalue_reference_t<std::add_const_t<std::decay_t<decltype(MPIHelper::getCommunication())>>>
      {
        return MPIHelper::getCommunication();
      }

    } // namespace Impl

    template<class T>
    auto comm(const T& t) -> decltype(auto) { return Impl::comm(t, PriorityTag<42>{}); }


    // common
    // ------

    void common () {}

    template<class T, class... Ts>
    auto common (T&& t, Ts&& ...)
      -> std::enable_if_t<Std::conjunction<std::is_same<T, Ts>...>::value, T&&>
    {
      return std::forward<T>(t);
    }

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_COMMON_UTILITY_HH
