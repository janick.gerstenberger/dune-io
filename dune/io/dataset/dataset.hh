#ifndef DUNE_IO_DATASET_DATASET_HH
#define DUNE_IO_DATASET_DATASET_HH

// std includes
#include <functional>
#include <type_traits>
#include <utility>


// dune-grid includes
#include <dune/grid/io/file/vtk/common.hh>

// dune-io includes
#include <dune/io/common/typetraits.hh>
#include <dune/io/common/utility.hh>
#include <dune/io/dataset/policy.hh>


namespace Dune
{

  namespace IO
  {

    // forward declaration
    // -------------------

    template< class, class, class > class DataSet;


    // DataSetBase
    // -----------

    template< class GV, class D, class P >
    struct DataSetBase
    {
      using GridView = GV;
      using Data = D;
      using Policy = P;

      constexpr auto policy () const noexcept { return Policy{}; }

      DataSetBase ( const GridView& gridView, const Data& data, const std::string& name, Policy = {} )
        : gridView_( gridView ), data_{ data }, name_( name )
      {}

      auto data () const noexcept -> const Data& { return data_.get(); }
      auto gridView () const noexcept -> const GridView& { return gridView_; }
      auto name () const noexcept -> const std::string& { return name_; }

    protected:
      static auto fieldInfo ( const std::string& name, std::size_t ncomps ) noexcept
      {
        using Type = typename VTK::FieldInfo::Type;
        return VTK::FieldInfo{ name, (ncomps > 1 ? Type::vector : Type::scalar ), ncomps };
      }

      GridView gridView_;
      std::reference_wrapper< const Data > data_;
      std::string name_;
    };


    // DataSet
    // -------

    namespace Impl
    {

      // DataSetPolicy
      // -------------

      template< class T, class = void >
      struct DataSetPolicy;

      template< class T >
      struct DataSetPolicy< T, std::enable_if_t< Policy::IsScalarOrVectorial< T >::value > > : public TypeOf< T > {};

      template < class T >
      struct DataSetPolicy< T, std::enable_if_t< IsSimpleFunctor< T >::value > > : public TypeOf< Policy::Transformation > {};

      template< class T >
      using DataSetPolicy_t = typename DataSetPolicy< T >::Type;


      // DataSetImpl
      // -----------

      template< class GV, class D, class P, class = void > class DataSetImpl;


      // specialization for Policy::Vectorial
      // ------------------------------------

      template< class GV, class D >
      class DataSetImpl< GV, D, DataSetPolicy< Policy::Vectorial > > : public DataSetBase< GV, D, Policy::Vectorial >
      {
        using This = DataSetImpl< GV, D, DataSetPolicy< Policy::Vectorial > >;
        using Base = DataSetBase< GV, D, Policy::Vectorial >;

      public:
        using Range = AddFieldVector_t< Range_t< GV, D > >;

        using Base::Base;
        using Base::name;

        auto fieldInfo () const { return Base::fieldInfo( name(), Range::dimension ); }
      };


      // specialization for Policy::Scalar
      // ---------------------------------

      template< class GV, class D >
      class DataSetImpl< GV, D, DataSetPolicy< Policy::Scalar > > : public DataSetBase< GV, D, Policy::Scalar >
      {
        using This = DataSetImpl< GV, D, DataSetPolicy< Policy::Scalar > >;
        using Base = DataSetBase< GV, D, Policy::Scalar >;

      public:
        using Range = AddFieldVector_t< Range_t< GV, D > >;

        using Base::Base;
        using Base::data;
        using Base::gridView;
        using Base::name;

        auto component ( std::size_t comp ) const
        {
          assert( comp < Range::dimension );
          auto t = [ comp ] ( const Range& value ) { return value[ comp ]; };
          return DataSet< GV, D, decltype( t ) >( gridView(), data(), name()+std::to_string( comp ), std::move( t ) );
        }

        auto fieldInfo () const { return Base::fieldInfo( name(), Range::dimension ); }
      };


      // specialization for Policy::Transformation
      // -----------------------------------------

      template< class T >
      using HasTransformationPolicy = Policy::IsTransformation< DataSetPolicy_t< T > >;


      template< class T, class GV, class D >
      class DataSetImpl< GV, D, DataSetPolicy< T >,
                         std::enable_if_t< HasTransformationPolicy< T >::value > >
        : public DataSetBase< GV, D, Policy::Transformation >
      {
        using This = DataSetImpl< GV, D, DataSetPolicy< T > >;
        using Base = DataSetBase< GV, D, Policy::Transformation >;

      public:
        using Range = AddFieldVector_t< ReturnType_t< T > >;

        using Base::name;

        DataSetImpl ( const GV& gridView, const D& data, const std::string& name, T t )
          :  Base{ gridView, data, name }, t_( std::move( t ) )
        {}

        auto fieldInfo () const { return Base::fieldInfo( name(), Range::dimension ); }
        auto transformation () const { return t_; }

      private:
        T t_;
      };

    } // namespace Impl

    template< class GV, class D, class PolicyOrFunctor >
    class DataSet : public Impl::DataSetImpl< GV, D, Impl::DataSetPolicy< PolicyOrFunctor > >
    {
      using Base = Impl::DataSetImpl< GV, D, Impl::DataSetPolicy< PolicyOrFunctor > >;

    public:
      using Base::Base;
    };


    // dataSet
    // -------

    template< class GV, class D, class PolicyOrFunctor = Policy::Vectorial >
    auto dataSet ( const GV& gridView, const D& data, const std::string& desc, PolicyOrFunctor p = {} )
      -> std::enable_if_t< IsGridView< GV >::value, DataSet< GV, D, PolicyOrFunctor > >
    {
      return DataSet< GV, D, PolicyOrFunctor >( gridView, data, (desc.empty() ? name( data ) : desc), std::move( p ) );
    }

    template< class D, class PolicyOrFunctor = Policy::Vectorial >
    decltype(auto) dataSet ( const D& data, const std::string& desc, PolicyOrFunctor p = {} )
    {
      return dataSet( gridView( data ), data, desc, std::move( p ) );
    }


  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_DATASET_DATASET_HH
