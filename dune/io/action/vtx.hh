#ifndef DUNE_IO_ACTION_VTX_HH
#define DUNE_IO_ACTION_VTX_HH

// STL includes
#include <iostream>
#include <string>
#include <utility>

// dune-grid includes
#include <dune/grid/io/file/vtk/common.hh>

#if HAVE_DUNE_FEM

// STL includes
#include <algorithm>
#include <list>
#include <map>
#include <memory>
#include <tuple>
#include <type_traits>

// dune-common includes
#include <dune/common/hybridutilities.hh>

// dune-grid includes
#include <dune/grid/io/file/vtk/vtkwriter.hh>

// dune-fem inclues
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/function/common/discretefunction.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/operator/projection/vtxprojection.hh>
#include <dune/fem/space/common/interpolate.hh>
#include <dune/fem/space/lagrange.hh>

// dune-io includes
#include <dune/io/action/vtk.hh>
#include <dune/io/common/utility.hh>
#include <dune/io/common/typeutility.hh>
#include <dune/io/dataset/dataset.hh>
#include <dune/io/dataset/view.hh>

#endif // #if HAVE_DUNE_FEM

namespace Dune
{

  namespace IO
  {

#if HAVE_DUNE_FEM

    namespace Impl
    {

      template< class ... DataSets >
      class VTXDependencies
      {
        using GridPartType = std::common_type_t< GridPart_t< typename DataSets::Data > ... >;

        template< class T >
        using FunctionSpace_t = typename std::decay_t< T >::FunctionSpaceType;

        template< class FS >
        using DiscreteSpace_t = Fem::LagrangeDiscreteFunctionSpace< FS, GridPartType, 1 >;

        struct Identity
        {
          template< class T >
          explicit Identity ( const T& obj ) : id_( static_cast< const void* >( &obj ) ) {}

          bool operator== ( const Identity& other ) const { return id_ == other.id_; }

          template< class T, std::enable_if_t< not std::is_same< Identity, T >::value, int > = 0 >
          bool operator== ( const T& other ) const { return id_ == &other; }
        private:
          const void* id_;
        };

        template< class T >
        using Storage_t = std::shared_ptr< T >;

        template< class DF >
        using StorageMap_t = std::list< std::pair< Identity, DF > >;

        using FunctionSpaceTuple    = UniqueTuple< FunctionSpace_t< typename DataSets::Data > ... >;

        using DiscreteSpaceTuple    = Decorate_t< DiscreteSpace_t, FunctionSpaceTuple >;
        using DiscreteFunctionTuple = Decorate_t< Fem::AdaptiveDiscreteFunction, DiscreteSpaceTuple >;

        using DiscreteSpaceStorage    = Decorate_t< Storage_t, DiscreteSpaceTuple >;
        using DiscreteFunctionStorage = Decorate_t< StorageMap_t, DiscreteFunctionTuple >;

      public:
        template< class ... Args >
        VTXDependencies ( Args&& ... args )
        {
           // allocate discrete spaces
          const auto& gridPart =  common( args.data().gridPart() ... );
          Hybrid::forEach( spaces_, [&] ( auto& space ) {
              space.reset( new std::decay_t< decltype( *space ) >{ const_cast< GridPartType& >( gridPart ) } );
            } );

           // allocate discrete functions & assign projections
          Hybrid::forEach( std::forward_as_tuple( args.data() ... ), [&] ( const auto& df ) {
            using DiscreteSpace = DiscreteSpace_t< FunctionSpace_t< decltype( df ) > >;
            const auto& space = *std::get< Storage_t< DiscreteSpace > >( spaces_ );
            auto& storage     = std::get< StorageMap_t< Fem::AdaptiveDiscreteFunction< DiscreteSpace > > >( functions_ );

            auto it = std::find_if( storage.begin(), storage.end(), [&] ( auto&& p ) { return p.first == df; } );
            if( it == storage.end() )
            {
              storage.emplace_back( std::piecewise_construct, std::forward_as_tuple( df ), std::forward_as_tuple( "", space ) );

              if ( df.space().continuous() )
                projections_.push_back( [ in = std::cref( df ), out = std::ref( storage.back().second ) ] () {
                    Fem::interpolate( in.get(), out.get() );
                  } );
              else
                projections_.push_back( [ in = std::cref( df ), out = std::ref( storage.back().second ) ] () {
                    Fem::WeightDefault< GridPartType > weight{};
                    Fem::VtxProjectionImpl::project( in.get(), out.get(), weight );
                  } );
            }
          } );
        }

        void project () { std::for_each( projections_.begin(), projections_.end(), [] ( auto&& proj ) { proj(); } ); }

        template< class DiscreteFunction >
        decltype(auto) function ( const DiscreteFunction& df ) const
        {
          using DF = Fem::AdaptiveDiscreteFunction< DiscreteSpace_t< typename DiscreteFunction::FunctionSpaceType > >;
          const auto& storage = std::get< StorageMap_t< DF > >( functions_ );

          auto it = std::find_if( storage.begin(), storage.end(), [&] ( auto&& p ) { return p.first == df; } );
          if ( it == storage.end() )
            DUNE_THROW( InvalidStateException, "Could not find function for vtx-projection" );
          return (it->second);
        }

      private:
        DiscreteSpaceStorage spaces_;
        DiscreteFunctionStorage functions_;
        std::list< std::function< void() > > projections_;
      };

      // Impl::addVTXData
      // ----------------

      template< class Writer, class Dependencies, class DataSet,
                std::enable_if_t< not Policy::IsTransformation< typename DataSet::Policy>::value, int > = 0 >
      void addVTXData ( std::shared_ptr< Writer >& writer, const std::shared_ptr< Dependencies >& deps, const DataSet& data )
      {
        addVertexData( writer, dataSet( data.gridView(), deps->function( data.data() ), data.name(), data.policy() ) );
      }

      template< class Writer, class Dependencies, class DataSet,
                std::enable_if_t< Policy::IsTransformation< typename DataSet::Policy >::value, int > = 0 >
      void addVTXData ( std::shared_ptr< Writer >& writer, const std::shared_ptr< Dependencies >& deps, const DataSet& data )
      {
        addVertexData( writer, dataSet( data.gridView(), deps->function( data.data() ), data.name(), data.transformation() ) );
      }


      // Impl::generateAction
      // --------------------

      template< class Writer, class Dependencies >
      auto generateAction ( std::shared_ptr< Writer >& writer, std::shared_ptr< Dependencies >& deps,
                            const std::string& name, VTK::OutputType type )
      {
        return [ vtx = std::make_pair( std::move( deps ), std::move( writer ) ), name, type ] ( const std::string& fragment, const std::string& path ) {
            vtx.first->project();
            vtx.second->pwrite( name + fragment, path, "", type );
          };
      }

    } // namespace Impl

#endif // #if HAVE_DUNE_FEM

    // vtxWriter
    // ---------

#if HAVE_DUNE_FEM

    auto vtxWriter( const std::string& name, VTK::OutputType type )
    {
      if ( Fem::Parameter::verbose() )
        std::cerr << "Warning: Call to vtxWriter( \"" << name << "\", ... ) is generating a no-op call." << std::endl;
      return [] ( const std::string&, const std::string& ) {};
    }

    template< class ... DataSets >
    decltype(auto) vtxWriter ( const std::string& name, VTK::OutputType type, DataSets&& ... dataSets )
    {
      static_assert(
          Std::conjunction< std::is_base_of< Fem::HasLocalFunction, typename std::decay_t< DataSets >::Data > ... >::value,
          "vtxWriter( ... ) only works with data set containing dune-fem functions."
        );

      auto gridView = common( dataSets.gridView() ... );
      auto writer   = std::make_shared< VTKWriter< decltype( gridView ) > >( gridView, VTK::conforming );
      auto deps     = std::make_shared< Impl::VTXDependencies< std::decay_t< DataSets > ... > >( dataSets ... );

      Hybrid::forEach( std::forward_as_tuple( dataSets ... ), [&] ( auto&& dataSet ) {
          Impl::addVTXData( writer, deps, std::forward< decltype( dataSet ) >( dataSet ) );
        } );

      return Impl::generateAction( writer, deps, name, type );
    }

#else // #if HAVE_DUNE_FEM

    template< class ... DataSets >
    decltype(auto) vtxWriter ( const std::string& name, VTK::OutputType type, DataSets&& ... dataSets )
    {
      std::cerr << "Warning: Vertex projection is only available with dune-fem,\n"
                << "         defaulting to vtkWriter( \"" << name << "\", VTK::conforming, ... ).";

      return vtkWriter( name, VTK::conforming, type, std::forward<  DataSets >( dataSets ) ... )
    }

#endif // #else // #if HAVE_DUNE_FEM

    template< class ... DataSets >
    decltype(auto) vtxWriter ( const std::string& name, DataSets&& ... dataSets )
    {
      return vtxWriter( name, VTK::ascii, std::forward< DataSets >( dataSets ) ... );
    }

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_ACTION_VTX_HH
