#ifndef DUNE_IO_DATASET_POLICY_HH
#define DUNE_IO_DATASET_POLICY_HH

// STL includes
#include <type_traits>

// dune-common includes
#include <dune/common/std/type_traits.hh>


namespace Dune
{

  namespace IO
  {

    namespace Policy
    {

      // Policy tags
      // -----------

      struct Scalar {};
      struct Vectorial {};
      struct Transformation {};


      // Helper Traits
      // -------------

      template< class T >
      using IsScalar = std::is_same< T, Scalar >;

      template< class T >
      using IsVectorial = std::is_same< T, Vectorial >;

      template< class T >
      using IsTransformation = std::is_same< T, Transformation >;

      template< class T >
      using IsScalarOrVectorial = Std::disjunction< IsScalar< T >, IsVectorial< T > >;

    } // namespace Policy


    namespace
    {
      constexpr Policy::Scalar scalar = {};
      constexpr Policy::Vectorial vectorial = {};
    }

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_DATASET_POLICY_HH
