#ifndef DUNE_IO_PREDICATES_FEM_HH
#define DUNE_IO_PREDICATES_FEM_HH

#if HAVE_DUNE_FEM

// STL includes
#include <limits>

// dune-fem includes
#include <dune/fem/io/parameter.hh>
#include <dune/fem/solver/timeprovider.hh>


namespace Dune
{

  namespace IO
  {

    struct FemDefault
    {
      FemDefault ( Fem::TimeProviderBase& tp, double saveStep )
        : FemDefault( tp, saveStep, 0 )
      {}

      FemDefault ( Fem::TimeProviderBase& tp, int saveCount )
        : FemDefault( tp, 0, saveCount )
      {}

      explicit FemDefault ( Fem::TimeProviderBase& tp, const Fem::ParameterReader& parameter = Fem::Parameter::container() )
        : FemDefault( tp, parameter.getValue< double >( "fem.io.savestep", 0 ), parameter.getValue< int >( "fem.io.savecount", 0 ) )
      {}

      FemDefault ( Fem::TimeProviderBase& tp, double saveStep, int saveCount )
        : tp_( tp ), ds_( saveStep ), n_( saveCount )
      {
        consistentSaveStep();
      }

      bool operator() () const
      {
        consistentSaveStep();

        const double eps = 2 * next_ * std::numeric_limits< double >::epsilon() ;
        const bool writeStep  = (ds_ > 0) && (tp_.time() - next_ >= -eps);
        const bool writeCount = (n_ > 0) && !(calls_ %= n_);

        // update state
        if ( writeStep )
          next_ += ds_;
        ++calls_;

        return writeStep || writeCount;
      }

    private:
      void consistentSaveStep () const
      {
        if( ds_ > 0 )
          for( const auto dist = tp_.time() - ds_; next_ <= dist; next_ += ds_ )
            continue;
      }

    protected:
      const Fem::TimeProviderBase& tp_;
      const double ds_;
      const int n_;
      mutable double next_ = 0.;
      mutable int calls_ = 0;
    };

  } // namespace IO

} // namespace Dune

#endif // #if HAVE_DUNE_FEM

#endif // #ifndef DUNE_IO_PREDICATES_FEM_HH
