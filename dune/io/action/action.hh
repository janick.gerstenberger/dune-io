#ifndef DUNE_IO_ACTION_ACTION_HH
#define DUNE_IO_ACTION_ACTION_HH

// STL includes
#include <functional>
#include <iomanip>
#include <sstream>
#include <string>
#include <utility>

// dune-common includes
#include <dune/common/std/type_traits.hh>


namespace Dune
{

  namespace IO
  {

    // BasicAction
    // -----------

    template< class F >
    class BasicAction
    {
      static_assert( Std::is_callable< F( const std::string&, const std::string& ), void >::value, "F does not provide a valid call." );

    public:
      explicit BasicAction ( F action = F{} )
        : action_( std::move( action ) )
      {}

      void operator() ( const std::string& suffix, const std::string& path )
      {
       action_( suffix, path );
      }

      void operator() ( int seqNum, const std::string& path )
      {
       action_( seqFragment( seqNum ), path );
      }

    private:
      std::string seqFragment ( int seqNum ) const
      {
        std::stringstream seq;
        seq.fill('0');
        seq << std::setw(5) << seqNum;
        return seq.str();
      }

    protected:
      F action_;
    };


    // Action
    // ------

    using Action = BasicAction< std::function<void( const std::string&, const std::string& )> >;

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_ACTION_ACTION_HH
