#ifndef DUNE_IO_ACTION_VTK_HH
#define DUNE_IO_ACTION_VTK_HH

// STL includes
#include <iostream>
#include <string>
#include <tuple>
#include <type_traits>
#include <utility>

// dune-common includes
#include <dune/common/hybridutilities.hh>
#include <dune/common/rangeutilities.hh>

// dune-grid includes
#include <dune/grid/io/file/vtk/common.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>

// dune-io includes
#include <dune/io/common/utility.hh>
#include <dune/io/dataset/dataset.hh>
#include <dune/io/dataset/policy.hh>
#include <dune/io/dataset/view.hh>


namespace Dune
{

  namespace IO
  {

    namespace Impl
    {

      // Impl::addCellData
      // -----------------

      template< class Writer, class DataSet,
                std::enable_if_t< not Policy::IsScalar< typename DataSet::Policy >::value, int > = 0 >
      void addCellData ( std::shared_ptr< Writer >& writer, const DataSet& dataSet )
      {
        writer->addCellData( dataSet, dataSet.fieldInfo() );
      }

      template< class Writer, class DataSet,
                std::enable_if_t< Policy::IsScalar< typename DataSet::Policy >::value, int > = 0 >
      void addCellData ( std::shared_ptr< Writer >& writer, const DataSet& dataSet )
      {
        for( auto i : range( dataSet.fieldInfo().size() ) )
          addCellData( writer, dataSet.component( i ) );
      }


      // Impl::addVertexData
      // -------------------

      template< class Writer, class DataSet,
                std::enable_if_t< not Policy::IsScalar< typename DataSet::Policy >::value, int > = 0 >
      void addVertexData ( std::shared_ptr< Writer >& writer, const DataSet& dataSet )
      {
        writer->addVertexData( dataSet, dataSet.fieldInfo() );
      }

      template< class Writer, class DataSet,
                std::enable_if_t< Policy::IsScalar< typename DataSet::Policy >::value, int > = 0 >
      void addVertexData ( std::shared_ptr< Writer >& writer, const DataSet& dataSet )
      {
        for( auto i : range( dataSet.fieldInfo().size() ) )
          addVertexData( writer, dataSet.component( i ) );
      }


      // Impl::addVTKData
      // ----------------

      template< class Writer, class DataSet >
      void addVTKData ( std::shared_ptr< Writer >& writer, const DataSet& dataSet )
      {
        if ( order( dataSet.data() ) == 0 )
          addCellData( writer, dataSet );
        else
          addVertexData( writer, dataSet);
      }


      // Impl::generateAction
      // --------------------

      template< class Writer >
      auto generateAction ( std::shared_ptr< Writer >& writer, const std::string& name, VTK::OutputType type )
      {
        return  [ vtk = std::move( writer ), name, type ] ( const std::string& fragment, const std::string& path ) {
            vtk->pwrite( name + fragment, path, "", type );
          };
      }

    } // namespace Impl


    // vtkWriter
    // ---------

    auto vtkWriter( const std::string& name, VTK::DataMode mode, VTK::OutputType type )
    {
      std::cerr << "Warning: vtkWriter( \"" << name << "\", ... ) is generating a no-op action." << std::endl;
      return [] ( const std::string&, const std::string& ) {};
    }

    template< class ... DataSets >
    decltype(auto) vtkWriter ( const std::string& name, VTK::DataMode mode, VTK::OutputType type, DataSets&& ... dataSets )
    {
      auto gridView = common( dataSets.gridView() ... );
      auto writer = std::make_shared< VTKWriter< decltype( gridView ) > >( gridView, mode );

      Hybrid::forEach( std::forward_as_tuple( dataSets ... ), [ &writer ] ( auto&& dataSet ) {
          Impl::addVTKData( writer, std::forward< decltype( dataSet ) >( dataSet ) );
        } );

      return Impl::generateAction( writer, name, type );
    }

    template< class ... DataSets >
    decltype(auto) vtkWriter ( const std::string& name, VTK::DataMode mode, DataSets&& ... dataSets )
    {
      return vtkWriter( name, mode, VTK::ascii, std::forward< DataSets >( dataSets ) ... );
    }

    template< class ... DataSets >
    decltype(auto) vtkWriter ( const std::string& name, VTK::OutputType type, DataSets&& ... dataSets )
    {
      return vtkWriter( name, VTK::nonconforming, type, std::forward< DataSets >( dataSets ) ... );
    }

    template< class ... DataSets >
    decltype(auto) vtkWriter ( const std::string& name, DataSets&& ... dataSets )
    {
      return vtkWriter( name, VTK::nonconforming, VTK::ascii, std::forward< DataSets >( dataSets ) ... );
    }


    // subsamplingVTKWriter
    // --------------------


    auto subsamplingVTKWriter( const std::string& name, int level, VTK::OutputType type )
    {
      std::cerr << "Warning: subsamplingVTKWriter( \"" << name << "\", ... ) is generating a no-op action." << std::endl;
      return [] ( const std::string&, const std::string& ) {};
    }

    template< class ... DataSets >
    decltype(auto) subsamplingVTKWriter ( const std::string& name, int level, VTK::OutputType type, DataSets&& ... dataSets )
    {
      auto gridView = common( dataSets.gridView() ... );
      auto writer   = std::make_shared< SubsamplingVTKWriter< decltype( gridView ) > >( gridView, level );

      Hybrid::forEach( std::forward_as_tuple( dataSets ... ), [ &writer ] ( auto&& dataSet ) {
          Impl::addVTKData( writer, std::forward< decltype( dataSet ) >( dataSet ) );
        } );

      return Impl::generateAction( writer, name, type );
    }

    template< class ... DataSets >
    decltype(auto) subsamplingVTKWriter ( const std::string& name, int level, DataSets&& ... dataSets )
    {
      return subsamplingVTKWriter( name, level, VTK::ascii, std::forward< DataSets >( dataSets ) ... );
    }

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_ACTION_VTK_HH
