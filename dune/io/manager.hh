#ifndef DUNE_IO_MANAGER_HH
#define DUNE_IO_MANAGER_HH

// STL includes
#include <functional>
#include <list>
#include <string>
#include <utility>

// dune-common includes
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpicommunication.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/optional.hh>

// dune-io includes
#include <dune/io/action/action.hh>
#include <dune/io/common/filesystem.hh>


namespace Dune
{

  namespace IO
  {

    // Manager
    // -------------

    template< class C = Communication< MPIHelper::MPICommunicator > >
    struct Manager
    {
      using Communication = C;
      using Predicate = std::function< bool() >;

      // Constructors w/o Communication
      // ----------------------------------------

      Manager ( Predicate predicate =  [] () { return true; } )
        : Manager( ".", 0, false, predicate )
      {}

      Manager ( bool verbose, Predicate predicate =  [] () { return true; } )
        : Manager( ".", 0, verbose, predicate )
      {}

      Manager ( const std::string& path, Predicate predicate =  [] () { return true; } )
        : Manager( path, 0, false, predicate )
      {}

      Manager ( const std::string& path, bool verbose, Predicate predicate =  [] () { return true; } )
        : Manager( path, 0, verbose, predicate )
      {}

      Manager ( const std::string& path, int count, bool verbose, Predicate predicate =  [] () { return true; } )
        : path_( path ), count_( count ), verbose_( verbose ), predicate_( std::move( predicate ) )
      {
        createPath( path, MPIHelper::getCommunication() );
      }

      // Constructors w/ Communication
      // -----------------------------------------

      Manager ( const Communication& comm, Predicate predicate =  [] () { return true; } )
        : Manager( comm, ".", 0, false, predicate )
      {}

      Manager ( const Communication& comm, bool verbose, Predicate predicate =  [] () { return true; } )
        : Manager( comm, ".", 0, verbose, predicate )
      {}

      Manager ( const Communication& comm, const std::string& path,
                Predicate predicate =  [] () { return true; } )
        : Manager( comm, path, 0, false, predicate )
      {}

      Manager ( const Communication& comm, const std::string& path, bool verbose,
                Predicate predicate =  [] () { return true; } )
        : Manager( comm, path, 0, verbose, predicate )
      {}

      Manager ( const Communication& comm,
                const std::string& path, int count, bool verbose,
                Predicate predicate =  [] () { return true; } )
        : comm_( &comm ), path_( path ), count_( count ), verbose_( verbose ),
          predicate_( std::move( predicate ) )
      {
        createPath( path, comm );
      }

      template< class ... Args >
      void add ( Args&& ... args )
      {
        Hybrid::forEach( std::forward_as_tuple( args ... ), [ & ] ( auto&& f ) {
            actions_.emplace_back( std::forward< decltype( f ) >( f ) );
          } );
      }

      bool operator() ( bool force =  false ) { return call( force ); }

      bool call ( bool force = false )
      {
        if ( !force && !predicate_() )
          return false;

        if ( verbose_ )
        {
          auto& comm = comm_ ? *comm_ : MPIHelper::getCommunication();
          std::cout << "Manager[" << comm.rank() << "]::action" << " #" << count_ << std::endl;
        }

        for( auto&& action : actions_ )
          action( count(), path() );
        ++count_;

        return true;
      }

      void clear () { actions_.clear(); }

      int count () const { return count_; }
      const std::string& path () const { return path_; }

    protected:
      static void createPath ( const std::string& path, const Communication& comm )
      {
        if( comm.rank() <= 0 && !directoryExists( path ) )
          createDirectory( path );
        comm.barrier();
      }

      const Communication* comm_;
      std::string path_;
      int count_;
      bool verbose_;
      Predicate predicate_;

      std::list< Action > actions_;
    };

  } // namespace IO

} // namespace Dune

#endif // #ifndef DUNE_IO_MANAGER_HH
